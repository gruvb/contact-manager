package com.cognitree.contactmanager.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public @Data class Contact {

    @Id
    private int id;
    @Column(name = "group_name")
    private String groupName;
    @Column(name = "name")
    private String name;
    @Column(name = "number")
    private String number;
    @Column(name = "city")
    private String city;

}
