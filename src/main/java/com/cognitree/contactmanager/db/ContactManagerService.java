package com.cognitree.contactmanager.db;

import com.cognitree.contactmanager.models.Contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ContactManagerService {

    @Autowired
    private ContactManagerRepository contactManagerRepository;

    public List<Contact> getAllRecords() {
        return (List<Contact>) contactManagerRepository.findAll();
    }

    public List<Contact> findByGroupName(Optional<String> groupName) {
        return new ArrayList<>(contactManagerRepository.findByGroupName(groupName));
    }

    public List<Contact> findByCityName(Optional<String> city) {
        return new ArrayList<>(contactManagerRepository.findByCity(city));
    }

    public Contact findById(int id) {
        return contactManagerRepository.findById(id);
    }

    public List<Contact> findByGroupNameAndCityName(Optional<String> groupName,
                                                    Optional<String> city) {
        return new ArrayList<>(contactManagerRepository.
                findByGroupNameAndCityName(groupName.get(), city.get()));
    }

    public void addContact(Contact contact) {
        contactManagerRepository.save(contact);
    }

    public List<Contact> findByName(String name) {
        return new ArrayList<>(contactManagerRepository.findByName(name));
    }

    public List<Contact> findByContactNumber(String oldContactNumber) {
        return new ArrayList<>(contactManagerRepository.findByNumber(oldContactNumber));
    }

    public void deleteName(String name) {
        contactManagerRepository.deleteByName(name);
    }

}