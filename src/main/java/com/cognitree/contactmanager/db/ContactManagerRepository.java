package com.cognitree.contactmanager.db;

import com.cognitree.contactmanager.models.Contact;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ContactManagerRepository extends CrudRepository<Contact, Long> {
    List<Contact> findByGroupName(Optional<String> groupName);

    List<Contact> findByCity(Optional<String> cityName);

    List<Contact> findByName(String name);

    List<Contact> findByNumber(String contactNumber);

    Contact findById(int id);

    void deleteByName(String name);

    @Query(value = "Select * from contact where group_name= :groupName and city= :city", nativeQuery = true)
    List<Contact> findByGroupNameAndCityName(@Param("groupName") String groupName, @Param("city") String city);

}
