package com.cognitree.contactmanager.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.cognitree.contactmanager.*"})
@EnableJpaRepositories("com.cognitree.contactmanager.*")
@EntityScan("com.cognitree.contactmanager.*")
public class ContactManagerJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContactManagerJpaApplication.class, args);

	}

}
