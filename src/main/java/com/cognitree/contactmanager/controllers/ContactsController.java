package com.cognitree.contactmanager.controllers;

import com.cognitree.contactmanager.db.ContactManagerService;
import com.cognitree.contactmanager.models.Contact;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/contacts")
public class ContactsController {

    private static final Logger logger = LoggerFactory.getLogger(ContactsController.class);

    @Autowired
    private ContactManagerService contactManagerService;

    @GetMapping("/")
    public List<Contact> getContacts(@RequestParam(name = "groupName", required = false)
                                                 Optional<String> groupName,
                                  @RequestParam(name = "city", required = false)
                                          Optional<String> city) {
        if (groupName.isPresent() && !city.isPresent()) {
            logger.debug("groupname displayed - {}", groupName);
            return contactManagerService.findByGroupName(groupName);
        }
        if (city.isPresent() && !groupName.isPresent()) {
            logger.info("cityname displayed - {}", city);
            return contactManagerService.findByCityName(city);
        }
        if (city.isPresent()) {
            logger.info("groupname and cityname displayed - {}, {}",groupName, city);
            return contactManagerService.findByGroupNameAndCityName(groupName, city);
        }
        logger.info("All records displayed");
        return contactManagerService.getAllRecords();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void addContact(@RequestBody Contact contact) {
        logger.info("Record added - {}, {}, {}, {}",
                contact.getCity(), contact.getName(), contact.getGroupName(), contact.getNumber());
        contactManagerService.addContact(contact);
    }

    @PutMapping("/{name}")
    public void updateContact(@PathVariable String name, @RequestBody Contact contact) {
        logger.info("Contact with name-{} updated", name);
        Contact oldContact = contactManagerService.findById(contact.getId());
        oldContact.setName(name);
        if(!contact.getNumber().isEmpty()) {
            oldContact.setNumber(contact.getNumber());
        }
        if(!contact.getCity().isEmpty()) {
            oldContact.setCity(contact.getCity());
        }
        if(!contact.getGroupName().isEmpty()) {
            oldContact.setGroupName(contact.getGroupName());
        }
        contactManagerService.addContact(oldContact);
    }

    @DeleteMapping("/{name}")
    public void deleteContact(@PathVariable(name = "name") String name) {
        logger.info("Deleted contact with name -{}", name);
        contactManagerService.deleteName(name);
    }

}
